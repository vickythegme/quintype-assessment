// quintypeAssessment Module
var app = angular.module('quintypeAssessment', ['ngResource', 'ngSanitize', 'ngRoute', 'ui.bootstrap']);

// quinTypeController
app.controller('quinTypeController', ['$scope', 'quinTypeStoriesFactory', function ($scope, quinTypeStoriesFactory) {
	$scope.logo = 'logo.png';
	$scope.liveUrl = "http://sketches.quintype.com/";	
	$scope.imgUrl = "http://quintype-01.imgix.net/";	
	$scope.loading = true;
	$scope.getStories = function() {
		quinTypeStoriesFactory.getResult($scope);
	}
	$scope.getStories();
}]);

// quinTypeStoriesFactory
app.factory('quinTypeStoriesFactory', function ($http) {
	var data = '';
	return { 
		getResult : function($scope){

			var url = $scope.liveUrl+'api/v1/stories';
			           // headers : { 'Content-Type': 'application/x-www-form-urlencoded', Access-Control-Allow-Origin: * }
			           headers = { 'Content-Type': 'application/x-www-form-urlencoded', 'Access-Control-Allow-Origin': '*' };
			           return $http.get(url, data, headers ).success(function (response) {
			           	$scope.stories = response.stories;
			           	$scope.loading = false;
			           });
			       } 
			   };

			});
//loadingDirective
app.directive('loading', [function () {
	var assetUrl = "assets/img/";
	return {
		restrict : 'E',
		replace : true,
		template : '<div class="loading"><img src="'+assetUrl+'loader.gif" width="20" height="20" /></div>',
		link: function (scope, iElement, iAttrs) {
			scope.$watch('loading', function(val) {
				if(val) {
					document.getElementsByClassName('loading')[0].style.display = "block";
				} else {
					document.getElementsByClassName('loading')[0].style.display = "none";

				}
			});
		}
	};
}]);