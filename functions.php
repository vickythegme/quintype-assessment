<?php
/*========================================
=            Define Constants            =
========================================*/
define('BASEURL', 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
define('JS_ASSETS', BASEURL."assets/js/");
define('CSS_ASSETS', BASEURL."assets/css/");
define('IMG_ASSETS', BASEURL."assets/img/");

/*=====  End of Define Constants  ======*/

/**
 * 
 * Function to show the Header
 *
 */

function vi_get_header() {
	?>
	<!doctype html>
	<html data-ng-app="quintypeAssessment">
	<head>
		<meta charset="utf-8">
		<meta name="description" content="Quintype Developer Assessment">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>QuinType Developer Assessment</title>
		<link rel="stylesheet" href="<?php echo CSS_ASSETS; ?>bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
		
		<link rel="stylesheet" href="<?php echo CSS_ASSETS; ?>style.css">
		<link rel="author" href="humans.txt">
	</head>
	<body data-ng-controller="quinTypeController" >

		<?php
	}

/**
 *
 * Function to show the Footer
 *
 */

function vi_get_footer() {
	?>
	<div class="container-fluid">
		<div class="text-center footer">
			<p>
				Copyright &copy; vickythegme <?php echo date('Y'); ?>
			</p>
		</div>
	</div>
	<script src="<?php echo JS_ASSETS; ?>modernizr-2.6.2.js"></script>
	<script type="text/javascript" src="<?php echo JS_ASSETS; ?>angular.min.js"></script>
	<script type="text/javascript" src="<?php echo JS_ASSETS; ?>angular-resource.min.js"></script>
	<script type="text/javascript" src="<?php echo JS_ASSETS; ?>angular-route.min.js"></script>
	<script type="text/javascript" src="<?php echo JS_ASSETS; ?>angular-sanitize.min.js"></script>
	<script type='text/javascript' src='<?php echo JS_ASSETS; ?>ui-bootstrap-tpls-2.1.1.js'></script>
	<script type='text/javascript' src='<?php echo JS_ASSETS; ?>ytmenu.js'></script>
	<script type="text/javascript" src="<?php echo JS_ASSETS; ?>app.js"></script>
</body>
</html>
<?php
}

/**
 *
 * Function to show the blocks
 *
 */

?>