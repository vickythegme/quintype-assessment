<?php
include_once('functions.php');
vi_get_header();
?>

<div class="container" >	
	<div class="container-fluid">

		<header class="navbar-fixed-top vi-fixed">

			<div class="col-md-12">
				<div class="col-md-3 col-sm-4 col-xs-5">
					<div class="dr-menu">
						<div class="dr-trigger" ng-click="getStories();"><span class="dr-icon dr-icon-menu"></span></div>
						<ul>
							<li><a class="dr-icon dr-icon-user" href="#">Vignesh A</a></li>
							<li><a class="dr-icon dr-icon-camera" href="#">Videos</a></li>
							<li><a class="dr-icon dr-icon-heart" href="#">Favorites</a></li>
							<li><a class="dr-icon dr-icon-bullhorn" href="#">Subscriptions</a></li>
							<li><a class="dr-icon dr-icon-download" href="#">Downloads</a></li>
							<li><a class="dr-icon dr-icon-settings" href="#">Settings</a></li>
							<li><a class="dr-icon dr-icon-switch" href="#">Logout</a></li>
						</ul>
					</div>
					<div class="vi-logo">
						<img ng-src="<?php echo IMG_ASSETS; ?>{{logo}}" alt="QuinType">
					</div>
				</div> <!-- Menu and Logo -->
				<div class="col-md-6 col-sm-4 col-xs-5">
					<input type="text" placeholder="Search..." class="vi-m-10 vi-search">
					<i class="fa fa-search vi-search-icon" aria-hidden="true"></i>
				</div> <!-- Search Box -->
				<div class="col-md-3 col-sm-4 col-xs-2 vi-m-10 vi-buttons ">
					<button class="btn vi-upload">Upload</button>
					<button class="btn vi-signin">Sign In</button>
				</div> <!-- SignIn -->
			</div>
		</header>
	</div> <!-- container-fluid -->

	<loading></loading>
</div><!-- /container -->
<div class="clearfix"></div>
<div class="container-fluid vi-m-100">
	<div class="col-md-2"></div>
	<div class="col-md-10 vi-main-container">
		<div class="vi-top-content vi-pb-10">
			<div class="col-md-7 col-sm-5  vi-main-showcase">
				<div class="vi-inner-main-showcase" ng-repeat="story in stories | limitTo:1 | orderBy:'published-at':true">
					<a href="{{liveUrl}}{{story.slug}}">
						<div class="col-md-8 vi-inner-img">
							<img ng-src="{{imgUrl}}{{ story['hero-image-s3-key']}}" width="300" height="200">
						</div>
					</a>
					<div class=" col-md-4 vi-inner-content">
						<a href="{{liveUrl}}{{story.slug}}">
							<h3>{{ story.headline }}</h3>
						</a>
						<div class="playlist clearfix">{{ story.sections[0].name }}</div>
						<div class="views">
							<ul>
								<li><span>{{story['author-name']}}</span></li>
								<li><span>{{story['last-published-at'] | date:'yyyy-mm-dd HH:mm:ss'}}</span></li>
							</ul>
						</div>
					</div>
				</div>
			</div> <!-- main showcase -->
			<div class="col-md-5 col-sm-6  vi-vertical-showcase">

				<div class="vi-inner-main-showcase clearfix" ng-repeat="story in stories" ng-if="$index > 0 && $index <= 4">
					<a href="{{liveUrl}}{{story.slug}}">
						<div class="col-md-5 vi-inner-img">
							<img ng-src="{{imgUrl}}{{ story['hero-image-s3-key']}}" width="100" height="100">
						</div>
					</a>
					<div class=" col-md-7 vi-inner-content">
						<a href="{{liveUrl}}{{story.slug}}">
							<h3>{{ story.headline }}</h3>
						</a>
						<div class="playlist clearfix">{{ story.sections[0].name }}</div>
						<div class="views">
							<ul>
								<li><span>{{story['author-name']}}</span></li>
								<li><span>{{story['last-published-at'] | date:'yyyy-mm-dd HH:mm:ss'}}</span></li>
							</ul>
						</div>
					</div>
				</div> <!-- showcase block -->
				
			</div>
		</div> <!-- top block -->
		<div class="clearfix"></div>
		<div class="vi-horizontal-showcase col-md-12">
			<div class="vi-horizontal-inner-showcase" ng-repeat="story in stories" ng-if="$index > 4">
				<a href="{{liveUrl}}{{story.slug}}">

					<div class="vi-inner-img">
						<img ng-src="{{imgUrl}}{{ story['hero-image-s3-key']}}" width="100" height="100">
					</div>
				</a>
				<div class="vi-inner-content">
					<a href="{{liveUrl}}{{story.slug}}">
						<h3>{{ story.headline }}</h3>
					</a>
					<div class="playlist clearfix">{{ story.sections[0].name }}</div>
					<div class="views">
						<ul>
							<li><span>{{story['author-name']}}</span></li>
							<li><span>{{story['last-published-at'] | date:'yyyy-mm-dd HH:mm:ss'}}</span></li>
						</ul>
					</div>
				</div>
			</div> <!-- showcase block -->

		</div>
	</div>

	<?php vi_get_footer();	?>
